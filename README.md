# Déform‘ Bitmap Vers Svg

Scripts s'inscrivant dans la proposition de l'Atelier Téméraire pour un Jeudi du Port à Brest en 2023.
Ces scripts seront utilisés dans les protocoles décrit [ici](https://gitlab.com/ateliertemeraire/jeudi-du-port-brest-2023-passerelle)

Pour la base de travail, nous avons repris les scripts produient par [Bonjour Monde](https://bonjourmonde.net/) nommé [Vinny](https://gitlab.com/bonjour-monde/tools/vinny/).

## Prérequis

+ Python 3
  + svgutils
  + xml
  + ElementTree
  + autotrace
+ bash
+ imagemagick

## Problèmes

Sur **macOS**, autotrace n'est plus disponible sur *pip* ou *homebrew*.

Il existe une release [ici](https://github.com/autotrace/autotrace), empaqueté dans une *.app*.

Une version est présente dans ce repository et une ligne dans le script principale y fait réfrence.

*[Mettre image d'illustration]*
